build:
	6c main.c && 6l main.6  && mv ./6.out 9fetch && rm main.6

run: build
	./9fetch
	
debug: build
	acid ./9fetch

install: build
	cp 9fetch /bin/9fetch
