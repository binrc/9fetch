#include <u.h>
#include <libc.h>

/* get uptime */
int
ut(void)
{
	char f[] = "/dev/time";
	long n;
	int fp;
	char buf[256];

	if((fp = open(f, OREAD)) < 0){
		perror(f);
		exits("file unreadable");
	}

	n = read(fp, buf, sizeof buf);

	if(n<0){
		perror(f);
		exits("file unreadable");
	}

	close(fp);
	
	/* split buf with whitespace delims */
	vlong f1 = atoll(strtok(buf, " "));
	vlong f2 = atoll(strtok(nil, " "));
	vlong f3 = atoll(strtok(nil, " "));
	vlong f4 = atoll(strtok(nil, " "));

	/* pretty print timestamp */
	print("%lld days, %02d:%02d:%02d\n", (f3/f4)/86400, (f3/f4)%86300/3600, (f3/f4)%86400%3600/60, (f3/f4)%60);
	
	return 0;
}

/* equiv of /etc/os-release */
int
mkos(void)
{	
	/* change this to your fork*/
	char *osname = "plan9front";
	int fp;

	/* if !exist, make /etc/os */
	if((fp = open("/env/os", OREAD)) < 0){
		fp = create("/env/os", OWRITE, 0644);
		write(fp, osname, strlen(osname));
	}
	
	close(fp);
	return 0;
}

/* basic file io */
int 
rf(char f[12])
{

	long n;
	int fp;
	char buf[1];

	if((fp = open(f, OREAD)) < 0){
		perror(f);
		exits("file unreadable");
	} 

	while((n=read(fp, buf, sizeof buf)) > 0)
		print("%c", buf[0]);

	if(n<0){
		perror(f);
		exits("file unreadable");
	}
	
	close(fp);
	
	return 0;
}

void 
main(int argc, char *argv[])
{
	/* need to make file */
	mkos();

	int art, i;
	char *farr[] = {"/env/user", "/env/sysname", "/env/os", "/env/cputype", "/env/vgasize", "/env/monitor"};
	char *glenda[] = {
			"(\\(\\      ", 
			"¸\". ..    ------------", 
			"(  . .)   OS: ", 
			"|   ° i   CPU: ", 
			"¿     \;   RES: ", 
			"c?\".UJ\"   DISP: ",
			"          UP: "};

	char *cirno[] = {
			"  (\\   /)    ",
			"  < (0) >    ------------",
			" (/ [ ] \\)   OS: ",
			"    / \\      CPU: ",
			"   /   \\     RES: ",
			"   ~~~~~     DISP: ",
			"             UP: "};



	/* input handler */
	if(argc == 1){
		art = 0;
	} else if(!strcmp(argv[1], "cirno")){
		art = 1;
	} else{ 
		art = 0;
	}

	
	/* format */
	print("\n");
	
	/* iterate through arrays, read files */
	for(i=0; i<7; i++){
		if(art == 0)
			print("%s", glenda[i]);
		
		if(art == 1)
			print("%s", cirno[i]);

		/* skip second read, first two on same line */
		if(i == 6){
			ut();
			break;
		} else if(i==0){
			rf(farr[i]);
			print("@");
			rf(farr[i+1]);
		} else if(i == 1){
		} else{
			rf(farr[i]);
		}
		print("\n");
	}

	/* format */
	print("\n");

	exits(0);
}

